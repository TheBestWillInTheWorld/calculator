package com.example.willgunby.calculator;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by willgunby on 15/03/15.
 *
 */
public class Calculator {


    //TODO: extend to cover more complex calcualtions, but for now, just use 2 operands and calculate on 2nd operator
    //Operand[] operands;

    Operand operand1;
    Operand operand2;
    Operator currentOperator;


    Calculator(){
        operand1 = new Operand();
        operand2 = new Operand();
        currentOperator = Operator.None;
    }

    public String textRepresentation(){
        String operator = "";
        if (currentOperator != Operator.None){
            operator = currentOperator.display();
        }
        return operand1.textRepresentation() + operator + operand2.textRepresentation();
    }


    public void operatorEntered(Operator operator){
        if (!operand1.isSealed){
            operand1.seal();
            currentOperator = operator;
        }
        else {
            evaluate();
            currentOperator = operator;
        }
    }

    public void evaluate(){
        if (currentOperator==Operator.None) return;
        operand1.setValue(calculate(operand1,currentOperator,operand2));
        System.out.println(operand1.value().toString());
        operand2.reset();
        currentOperator=Operator.None;
    }


    public void operandEntered(Character operandChar){
        //todo: validate & restrict entry
        if (!operand1.isSealed) {
            operand1.newChar(operandChar);
        }
        else {
            if (currentOperator==Operator.None){
                operand1.reset();
                operand1.newChar(operandChar);
            }
            else {
                operand2.newChar(operandChar);
            }
        }
    }

    private BigDecimal calculate(Operand o1, Operator operator, Operand o2){
        switch (operator){
            case Divide     : return o1.value().divide  (o2.value());
            case Multiply   : return o1.value().multiply(o2.value());
            case Add        : return o1.value().add(o2.value());
            case Subtract   : return o1.value().subtract(o2.value());
        }
       return o1.value();
    }


    public enum Operator{
        //fancy enum to allow us to store an alternative representation for each Enum value
        None     ("") ,
        Divide   ("/"),
        Multiply ("*"),
        Add      ("+"),
        Subtract ("-");

        private final String displayAs;
        private Operator(String displayAs){
            this.displayAs = displayAs;
        }
        public final String display(){
            return displayAs;
        }
    }


    public class Operand {

        public BigDecimal value() {
            return valueRightAsNumber().add(new BigDecimal(valueLeft));
        }
        private void setValue(BigDecimal value) {
            valueLeft = value.intValue();
            BigDecimal valRight = value.subtract(new BigDecimal(valueLeft));

            //if not right side value==0
            if (valRight.compareTo(BigDecimal.ZERO) != 0){ hasDecimal = true;  isRightSet = true;    }
            valueRight = valRight.toString().toCharArray();
        }

        public String textRepresentation(){
            if (isLeftSet){
                //if whole number, don't return the decimal
                if (!isRightSet || valueRightAsNumber().compareTo(BigDecimal.ZERO)==0) {
                    return valueLeft.toString();
                }
                else {
                    BigDecimal value = valueRightAsNumber().add(new BigDecimal(valueLeft));
                    return value.toString();
                }
            }
            return "";
        }

        private BigDecimal valueRightAsNumber(){
            String stringRep = new String(valueRight);
            DecimalFormat dec = new DecimalFormat();
            dec.setParseBigDecimal(true);

            return new BigDecimal(stringRep);
        }

        //store 2 side of the decimal seperately to make building the values from a string easier
        private Integer valueLeft;
        private Boolean hasDecimal;
        private char[] valueRight;


        private Boolean isSealed;
        private Boolean isLeftSet;
        private Boolean isRightSet;


        Operand(){
            reset();
        }

        public void reset(){
            isSealed    = false;
            isLeftSet   = false;
            isRightSet  = false;
            hasDecimal  = false;
            setValue(new BigDecimal(0.0));
        }

        public void newChar(Character newChar){
            if (!isRightSet && newChar == '.') {
                hasDecimal= true;
                isLeftSet = true;
            }
            else {
                String val;
                if (hasDecimal) {
                    if (!isRightSet) {  val = "0.";                  }
                    else {              val = new String(valueRight);}
                    valueRight = (val+newChar).toCharArray();
                    isRightSet = true;
                }
                else {
                    if (!isLeftSet) {  val = "";                  }
                    else {              val = valueLeft.toString();}
                    valueLeft = Integer.parseInt(val + newChar);
                    isLeftSet = true;
                }
            }
        }

        public void seal(){
            isSealed = true;
        }
    }

}
