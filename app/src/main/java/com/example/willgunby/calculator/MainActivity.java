package com.example.willgunby.calculator;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends ActionBarActivity {


    EditText editCalculation;

    Button btn1;    Button btn2;        Button btn3;        Button btnDivide;
    Button btn4;    Button btn5;        Button btn6;        Button btnMultiply;
    Button btn7;    Button btn8;        Button btn9;        Button btnAdd;
    Button btn0;    Button btnDecimal;  Button btnEquals;   Button btnSubtract;

    Calculator calc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editCalculation = (EditText) findViewById(R.id.editCalculation);

        calc = new Calculator();

        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnDecimal  = (Button) findViewById(R.id.btnDecimal);

        btnDivide   = (Button) findViewById(R.id.btnDivide);
        btnMultiply = (Button) findViewById(R.id.btnMultiply);
        btnAdd      = (Button) findViewById(R.id.btnAdd);
        btnSubtract = (Button) findViewById(R.id.btnSubtract);
        btnEquals   = (Button) findViewById(R.id.btnEquals);

        btnEquals.setOnClickListener    (new View.OnClickListener() { @Override public void onClick(View v) {   operatorClick(v);  }});
        btnDivide.setOnClickListener    (new View.OnClickListener() { @Override public void onClick(View v) {   operatorClick(v);  }});
        btnMultiply.setOnClickListener  (new View.OnClickListener() { @Override public void onClick(View v) {   operatorClick(v);  }});
        btnAdd.setOnClickListener       (new View.OnClickListener() { @Override public void onClick(View v) {   operatorClick(v);  }});
        btnSubtract.setOnClickListener  (new View.OnClickListener() { @Override public void onClick(View v) {   operatorClick(v);  }});

        btn0.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn1.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn2.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn3.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn4.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn5.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn6.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn7.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn8.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btn9.setOnClickListener         (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});
        btnDecimal.setOnClickListener   (new View.OnClickListener() { @Override public void onClick(View v) {   operandClick(v);  }});

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void operatorClick(View v) {
        if (v==btnEquals)   {   calc.evaluate();                                      }
        if (v==btnDivide)   {   calc.operatorEntered(Calculator.Operator.Divide);     }
        if (v==btnMultiply) {   calc.operatorEntered(Calculator.Operator.Multiply);   }
        if (v==btnAdd)      {   calc.operatorEntered(Calculator.Operator.Add);        }
        if (v==btnSubtract) {   calc.operatorEntered(Calculator.Operator.Subtract);   }

        editCalculation.setText(calc.textRepresentation());
    }

    public void operandClick(View v) {
        if (v==btn0)         {   calc.operandEntered('0');     }
        if (v==btn1)         {   calc.operandEntered('1');     }
        if (v==btn2)         {   calc.operandEntered('2');     }
        if (v==btn3)         {   calc.operandEntered('3');     }
        if (v==btn4)         {   calc.operandEntered('4');     }
        if (v==btn5)         {   calc.operandEntered('5');     }
        if (v==btn6)         {   calc.operandEntered('6');     }
        if (v==btn7)         {   calc.operandEntered('7');     }
        if (v==btn8)         {   calc.operandEntered('8');     }
        if (v==btn9)         {   calc.operandEntered('9');     }
        if (v==btnDecimal)   {   calc.operandEntered('.');     }

        editCalculation.setText(calc.textRepresentation());
    }


}

